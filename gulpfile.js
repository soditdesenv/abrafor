const gulp = require('gulp')

require('./gulpTasks/app')
require('./gulpTasks/deps')
require('./gulpTasks/server')


gulp.task('default', function(){
    gulp.start('app', 'deps','server');
})